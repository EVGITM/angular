import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class BooksService {



  books = [
    {title:'Alice in Wonderland', author:'Lewis Carrol',summary:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley"},
    {title:'War and Peace', author:'Leo Tolstoy',summary:"the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"}, 
    {title:'The Magic Mountain', author:'Thomas Mann',summary:"the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"}];


    public addBooks(){
      setInterval(()=>this.books.push({title:'The Magic Mountain', author:'Thomas Mann',summary:"the 1960s wer including versions of Lorem Ipsum"}),2000)

    }
/*
    public getBooks(){
     const booksObservavle = new Observable(observer => {
      setInterval(()=>observer.next(this.books),2000)
     });

     return booksObservavle;
    }*/

  

/*

    public getBooks(){
      return this.books;

    }
    

    getBooks(userId):Observable<any>{

    } */
  constructor(private db: AngularFirestore) { }

  bookCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection(`users`);

  
  /* public getBooks(userId){
    this.bookCollection = this.db.collection(`users/${userId}/books`);
    return this.bookCollection.snapshotChanges().pipe(map(
      collection => collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )
    ))
   }*/

   public getBooks(userId, startAfter){
    this.bookCollection = this.db.collection(`users/${userId}/books`, 
    ref => ref.orderBy('title', 'asc').limit(4).startAfter(startAfter));     
    return this.bookCollection.snapshotChanges();
   }

deleteBook( userId:string ,id:string ){
  this.db.doc(`users/${userId}/books/${id}`).delete();

}

updateBook(userId:string,id:string,title:string,author:string){
  this.db.doc(`users/${userId}/books/${id}`).update(
    {
      title:title,
      author:author
    }
  )
}

//if u dont set id for the boook it will generate automatic
addBook(userId:string, title:string,author:string){
  const book  = {title:title, author:author} 
  this.userCollection.doc(userId).collection('books').add(book);
  // anothe way -> this.db.collection(`users/${userId}/books/`).add(book);
}

}

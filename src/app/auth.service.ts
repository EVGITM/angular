import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user:Observable<User | null>; 

  constructor(private afAuth:AngularFireAuth, private router:Router ) { 
    this.user = this.afAuth.authState; 
  }

  login(email:string, password:string){
    return this.afAuth.signInWithEmailAndPassword(email, password)
  }

  signup(email:string, password:string){
    return this.afAuth.createUserWithEmailAndPassword(email, password).then(res => {
      console.log('Sign Up Successful');
      this.router.navigate(['/books']); 
    })
  
  }

  logout(){
    this.afAuth.signOut();
    console.log("logout Successful");

    }

    getUser():Observable<User| null> {
      return this.user

    }
  
}

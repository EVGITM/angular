import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Weather } from './interfaces/weather';
import { WeatherRaw } from './interfaces/weather-raw';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private apiKey= "c978c1afe3f7ef18aa84ae9b46c3704e";
  private IMP = "units=metric";


  constructor(private http:HttpClient) { }

  searchWeatherData(cityName:String):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.apiKey}&${this.IMP}`).pipe(
      map(data => this.transformWeatherData(data)), 
      catchError(this.handleError)
    )

  }

  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server Error')
  }

  private transformWeatherData(data:WeatherRaw):Weather{
    return {
      name:data.name,
      country:data.sys.country,
      image:`https://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      description:data.weather[0].description,
      temperature:data.main.temp,
      lat: data.coord.lat,
      lon: data.coord.lon,


    }
  }


}

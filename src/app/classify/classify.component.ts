import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {
  networks:Array<string>  = ['bbc', 'cnn', 'nbc'];
  url_network ;

  constructor(private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.url_network = this.route.snapshot.params.network;
  }

}

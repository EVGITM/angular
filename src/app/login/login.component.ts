import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password:string;
  hasError:boolean = false;
  errorMassage:string;

  onSubmit(){
    this.Auth.login(this.email, this.password).then(res => {
      console.log("Login Successful");
      this.router.navigate(['/books']); 
    }).catch(() => {
      this.hasError=true;
      this.errorMassage = 'Email or Password is not correct, please check them and try again';
    })
  }

  constructor(private Auth:AuthService ,private router:Router) { }
  

  
  ngOnInit(): void {
  }

}

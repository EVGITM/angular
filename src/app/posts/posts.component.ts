import { Observable } from 'rxjs';
import { BlogPostService} from '../blog-post.service';
import { Component, OnInit } from '@angular/core';
import { BlogPost } from '../interfaces/blog-post';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  posts;
  postData$:Observable<BlogPost>;
  constructor(private blogService: BlogPostService) { }

  ngOnInit(): void {
    this.postData$ = this.blogService.getBlog();
    this.postData$.subscribe(data => this.posts = data);

  }

}
export interface BlogPost {
    title: string,
    body: string,
    id: number,
    userId: number

}

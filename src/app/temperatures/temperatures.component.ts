import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Weather } from '../interfaces/weather';
import { WeatherService } from '../weather.service';



@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {
  city:string;
  temperature:number;
  image;
  weatherData$:Observable<Weather>;
  hasError:boolean = false;
  errorMassage:string;

  country:string;
  longitude:number;
  latitude:number;
  data:any;

  constructor(private route:ActivatedRoute, private weatherService:WeatherService) { }

  ngOnInit(): void {
    this.city = this.route.snapshot.params.city;
    this.weatherData$ = this.weatherService.searchWeatherData(this.city)
    this.weatherData$.subscribe(
    (data) => {this.temperature = Math.round(data.temperature);
    this.image = data.image ;
    this.country = data.country;
    this.longitude= data.lon;
    this.latitude= data.lat;

  },
  (error) => {
      console.log(error.message);
      this.hasError = true;
      this.errorMassage = error.message;
    }
  )
  }

}

import { Book } from './../interfaces/book';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';

@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  //books;
  books$;
  userId:string; 
  editstate:Array <boolean> = [];
  book:Book;
  addBookFormOPen:boolean = false;
  books:Array <Book>
  lastDocumentArrived;
  LowerPageLimit;


  panelOpenState = false;

  constructor(private booksService:BooksService, public Auth:AuthService) { 
  }

  deleteBook(id:string){
    this.booksService.deleteBook(this.userId, id);
  }

  update(book:Book){
    this.booksService.updateBook(this.userId, book.id, book.title, book.author)
  }

  add(book:Book){
    this.booksService.addBook(this.userId, book.title, book.author)
  }





/*we can send index numver or the document itself -> look lastDocumentArrived vs  LowerPageLimit */
  nextPage(){
    this.books$ = this.booksService.getBooks(this.userId, this.lastDocumentArrived);
    this.books$.subscribe(
     docs =>{
      this.LowerPageLimit = docs.length-1; 

      this.lastDocumentArrived = docs[docs.length-1].payload.doc; 

      this.books = [];
       for(let document of docs){
         const book:Book = document.payload.doc.data();
         book.id = document.payload.doc.id; 
         this.books.push(book); 
       }
     }
   )


  }

  previousPage(){
    this.books$ = this.booksService.getBooks(this.userId, this.LowerPageLimit);
    this.books$.subscribe(
     docs =>{
      this.lastDocumentArrived = docs[docs.length-1].payload.doc; 
      this.books = [];
       for(let document of docs){
         const book:Book = document.payload.doc.data();
         book.id = document.payload.doc.id; 
         this.books.push(book); 
       }
     }
   )


  }




  ngOnInit(): void {
  
   this.Auth.getUser().subscribe(
     (user) =>{
       this.userId = user.uid;
       console.log(this.userId);
       this.books$ = this.booksService.getBooks(this.userId, null);
       this.books$.subscribe(
        docs =>{
          this.LowerPageLimit = 0; 

          this.lastDocumentArrived = docs[docs.length-1].payload.doc; 
          this.books = [];
          for(let document of docs){
            const book:Book = document.payload.doc.data();
            book.id = document.payload.doc.id; 
            this.books.push(book); 
          }
        }
      ) 
    }
  )

}
   


  }






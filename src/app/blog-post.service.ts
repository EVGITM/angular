import { Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BlogPost } from './interfaces/blog-post';

@Injectable({
  providedIn: 'root'
})
export class BlogPostService {
  private URL :string = "https://jsonplaceholder.typicode.com/posts/";
  
  constructor(private http:HttpClient) { }

  getBlog():Observable<BlogPost>{
    return this.http.get<BlogPost>(`${this.URL}`);
  }

  

  // private handleError(res:HttpErrorResponse){
  //   console.log(res.error);
  //   return throwError(res.error || 'Server Error')
 
  

}
